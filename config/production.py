# -*- coding: utf-8 -*-
import os
from edwapp import app


DEBUG = False
SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
SECRET_KEY = 'generate_good_secure_key os.urandom(24).hex()'
SESSION_COOKIE_SAMESITE = 'Strict'
SQLALCHEMY_DATABASE_URI = 'mssql+pyodbc://webAppAdmin:webAppAdmin@sqlserverdatasource'
#SQLALCHEMY_DATABASE_URI = 'mssql+pyodbc://webAppAdmin:webAppAdmin@testdb'
WTF_CSRF_ENABLED = True
BABEL_TRANSLATION_DIRECTORIES = os.path.join(os.path.dirname(os.path.normpath(app.root_path)), 'translations')
BABEL_DEFAULT_LOCALE = 'cs'
LANGUAGES = {
    'en': 'English',
    'cs': 'Česky'
}
#pokayoka defaults start
POKAYOKA_PRODITEM = ''
POKAYOKA_PRODESCR = ''
POKAYOKA_TOTAL = 0
POKAYOKA_LEFTOVER = 0
POKAYOKA_SUBDATA = ()
#pokayoka end