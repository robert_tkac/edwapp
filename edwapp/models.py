# -*- coding: utf-8 -*-
#!/usr/bin/env python3

import datetime
from sqlalchemy import (Table, Column, String, Integer, DateTime, Text, CheckConstraint, Boolean, ForeignKey, Float)
from sqlalchemy.orm import relationship, aliased
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from . import database

###################################Transport start#######################################################

association_table = Table('t_userroles', database.Base.metadata,
    Column('user_id', Integer, ForeignKey('t_users.userid', ondelete='CASCADE')),
    Column('role_id', Integer, ForeignKey('t_roles.roleid', ondelete='CASCADE'))
)


class Users(UserMixin, database.Base):
    '''
    #https://flask-login.readthedocs.io/en/latest/_modules/flask_login/mixins.html
    '''
    __tablename__ = 't_users'
    __table_args__ = ({'sqlite_autoincrement': True})
    userid = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(String(100), nullable=False)
    surname = Column(String(100), nullable=False)
    usertel = Column(String(20))    
    useremail = Column(String(255), unique=True, nullable=False)
    password_hash = Column(String(255), nullable=False)
    active = Column(Boolean(), nullable=False, default=1)
    deleted = Column(Boolean(), nullable=False, default=0)
    roles = relationship('Roles', secondary=association_table, back_populates='users')
    _is_authenticated = True
    _is_anonymous = False
        
    def __repr__(self):
        return '{0}, {1}'.format(self.surname, self.name)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password, method='sha256')

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)
    
    def get_id(self):
        try:
            return str(self.userid)
        except AttributeError:
            raise NotImplementedError('No `id` attribute - override `get_id`')
    
    @property
    def is_authenticated(self):
        return self._is_authenticated
    
    @is_authenticated.setter
    def is_authenticated(self, value):
        self._is_authenticated = value
    
    @property
    def is_active(self):
        if self.deleted:
            return False
        return self.active 
    
    @is_active.setter
    def is_active(self, value):
        self.active = value

    @property
    def is_anonymous(self):
        return self._is_anonymous
    
    @is_anonymous.setter
    def is_anonymous(self, value):
        self._is_anonymous = value


class Roles(database.Base):
    __tablename__ = 't_roles'
    __table_args__ = ({'sqlite_autoincrement': True})
    roleid = Column(Integer, autoincrement=True, primary_key=True)
    rolename = Column(String(50), unique=True)
    rolearea = Column(String(50))
    users = relationship('Users', secondary=association_table, back_populates='roles')

    def __repr__(self):
        return '{0}'.format(self.rolename)


class TransportConfigT(database.Base):
    __tablename__ = 't_transportconfig'
    cid = Column(Integer, primary_key = True)
    clabel = Column(String(100), unique=True, nullable=False)
    cvalue = Column(Text)


class TransportOrdersT(database.Base):
    __tablename__ = 't_transportorders'
    __table_args__ = ({'sqlite_autoincrement': True})
    orderid = Column(Integer, autoincrement=True, primary_key=True)
    orderTitle = Column(String(100), nullable=False)
    userid = Column(Integer, ForeignKey('t_users.userid'), nullable=False)
    users = relationship('Users', foreign_keys=[userid])
    orderCreationDate = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    pickupDate = Column(DateTime, nullable=False)
    pickupPlace = Column(Integer, ForeignKey('t_transportplaces.placeid'), nullable=False)
    pickplaces = relationship('TransportPlacesT', foreign_keys=[pickupPlace])
    pickupContactid = Column(Integer, ForeignKey('t_transportplacescontacts.contactid'))
    pickuplacescontacts = relationship('TransportPlacesContactsT', foreign_keys=[pickupContactid])
    deliveryDate = Column(DateTime, nullable=False)
    deliveryPlace = Column(Integer, ForeignKey('t_transportplaces.placeid'), nullable=False)
    deliveryplaces = relationship('TransportPlacesT', foreign_keys=[deliveryPlace])
    deliveryContactid = Column(Integer, ForeignKey('t_transportplacescontacts.contactid'))
    deliveryplacescontacts = relationship('TransportPlacesContactsT', foreign_keys=[deliveryContactid])
    orderNotes = Column(Text)
    orderview = Column(Text)
    winnerEnteredBy = Column(Integer, ForeignKey('t_users.userid'))
    winnerEneters = relationship('Users', foreign_keys=[winnerEnteredBy])
    transportWinner = Column(Integer, ForeignKey('t_transportplaces.placeid'))
    transportWinners = relationship('TransportPlacesT', foreign_keys=[transportWinner])
    winnerEnetredDate = Column(DateTime)
    price = Column(Integer)
    currency = Column(String(3), ForeignKey('t_geocurrencies.currencycode'))
    currencies = relationship('GeoCurrencies', foreign_keys=[currency])
    looserprice = Column(Integer)    
    loosercurrency = Column(String(3), ForeignKey('t_geocurrencies.currencycode'))
    loosercurrencies = relationship('GeoCurrencies', foreign_keys=[loosercurrency])
    choiceReason = Column(Text)
    approvedby1 = Column(Integer, ForeignKey('t_users.userid'))
    approvers1 = relationship('Users', foreign_keys=[approvedby1])
    orderApprovalDate1 = Column(DateTime)
    approvedby2 = Column(Integer, ForeignKey('t_users.userid'))
    approvers2 = relationship('Users', foreign_keys=[approvedby2])
    orderApprovalDate2 = Column(DateTime)
    canceledby = Column(Integer, ForeignKey('t_users.userid'))
    cancelators = relationship('Users', foreign_keys=[canceledby])
    cancelationReason = Column(Text) 
    orderCancelationDate = Column(DateTime)


class TransportItemsT(database.Base):
    __tablename__ = 't_transportitems'
    __table_args__ = (CheckConstraint('goodsType in (0,1,2)'), 
                      CheckConstraint('stackable in (0,1)'), 
                      {'sqlite_autoincrement': True})
    itemid = Column(Integer, autoincrement=True, primary_key=True)
    orderid = Column(Integer, ForeignKey('t_transportorders'), nullable=False)
    orders = relationship('TransportOrdersT')#, backref = 'orderitems')
    description = Column(String(50), nullable=False)
    pieces = Column(Integer, nullable=False)
    sizewidth = Column(Integer, nullable=False)
    sizelength = Column(Integer, nullable=False)
    sizeheight = Column(Integer, nullable=False)
    weight = Column(Integer, nullable=False)
    goodsType = Column(Integer, nullable=False)
    stackable = Column(Integer, nullable=False)


class TransportPlacesT(database.Base):
    __tablename__ = 't_transportplaces'
    __table_args__ = (CheckConstraint('deleted in (0,1)'), CheckConstraint('hauler in (0,1)'),{'sqlite_autoincrement': True})
    placeid = Column(Integer, autoincrement=True, primary_key=True)
    friendlyname = Column(String(100), nullable=False, unique=True)
    name = Column(String(100), nullable=False)
    name2 = Column(String(100))
    address1 = Column(String(120), nullable=False)
    streetnumber = Column(String(20))
    address2 = Column(String(120))    
    area = Column(String(120))
    city = Column(String(120), nullable=False)
    zip = Column(String(20), nullable=False)
    countryid = Column(Integer, ForeignKey('t_geocountries.countryid'), nullable=False)
    countries = relationship('GeoCountries')
    hauler = Column(Integer, nullable=False, default=0)
    deleted = Column(Integer, nullable=False, default=0)
    
    def __repr__(self):
        return '{0}'.format(self.friendlyname)


class TransportPlacesContactsT(database.Base):
    __tablename__ = 't_transportplacescontacts'
    __table_args__ = (CheckConstraint('deleted in (0,1)'), CheckConstraint('restricted in (0,1)'), {'sqlite_autoincrement': True})
    contactid = Column(Integer, autoincrement=True, primary_key=True)
    contactname = Column(String(100))
    contacttel = Column(String(100))
    contactemail = Column(String(100))
    placeid = Column(Integer, ForeignKey('t_transportplaces.placeid'))
    places = relationship('TransportPlacesT')
    restricted = Column(Integer, nullable=False, default=0)
    deleted = Column(Integer, nullable=False, default=0)

    def __repr__(self):
        return '{0} {1} {2}'.format(self.contactname, self.contacttel, self.contactemail)


class GeoContinents(database.Base):
    __tablename__ = 't_geocontinents'
    continentcode = Column(String(2), primary_key=True)
    continentname = Column(String(50), nullable=False, unique=True)

    def __init__(self, continentcode, continentname):
        self.continentcode = continentcode
        self.continentname = continentname

    def __repr__(self):
        return '({},{})'.format(self.continentcode, self.continentname)


class GeoSubregions(database.Base):
    __tablename__ = 't_geosubregions'
    subregioncode = Column(Integer, primary_key=True)
    subregionname = Column(String(50), nullable=False, unique=True)

    def __init__(self, subregioncode, subregionname):
        self.subregioncode = subregioncode
        self.subregionname = subregionname

    def __repr__(self):
        return '({},{})'.format(self.subregioncode, self.subregionname)
    

class GeoCountries(database.Base):
    __tablename__ = 't_geocountries'
    countryid = Column(Integer, autoincrement=True, primary_key=True)
    countryname = Column(String(80), nullable=False)
    iso3166alpha2 = Column(String(2), nullable=False, unique=True)
    iso3166alpha3 = Column(String(3), nullable=False, unique=True)
    iso3166numeric = Column(Integer, nullable=False, unique=True)
    continentcode = Column(String(2), ForeignKey('t_geocontinents.continentcode'), nullable=False)   
    continents = relationship('GeoContinents')
    subregioncode = Column(Integer, ForeignKey('t_geosubregions.subregioncode'), nullable=False)
    subregions = relationship('GeoSubregions')
    iseu = Column(Integer, nullable=False, default=0)
    isshengen = Column(Integer, nullable=False, default=0)
    
    def __init__(self, countryname, iso3166alpha2, iso3166alpha3, iso3166numeric, continentcode, subregioncode, iseu=0, isshengen=0):
        self.countryname = countryname
        self.iso3166alpha2 = iso3166alpha2
        self.iso3166alpha3 = iso3166alpha3
        self.iso3166numeric = iso3166numeric
        self.continentcode = continentcode
        self.subregioncode = subregioncode
        self.iseu = iseu
        self.isshengen = isshengen

    def __repr__(self):
        return '{0}, {1}'.format(self.countryname, self.iso3166alpha3)


class GeoCurrencies(database.Base):
    __tablename__ = 't_geocurrencies'
    currencycode = Column(String(3), primary_key=True)
    currencyname = Column(String(50), nullable=False, unique=True)

    def __init__(self, currencycode, currencyname):
        self.currencycode = currencycode
        self.currencyname = currencyname

    def __repr__(self):
        return '{}, {}'.format(self.currencycode, self.currencyname)


#class ExRates(database.Base):
#    __tablename__ = 't_exrates'
#    exrateid = Column(Integer, autoincrement=True, primary_key=True)
#    exratecurrencycode = Column(String(3), ForeignKey('t_geocurrencies.currencycode'), nullable=False)
#    currencycodes = relationship('GeoCurrencies')
#    exratevalue = Column(Float(), nullable=False)
#    exrateyear = Column(Integer, nullable=False)


users_01 = aliased(Users)
pickPlaces = aliased(TransportPlacesT)
delPlaces = aliased(TransportPlacesT)