# -*- coding: utf-8 -*-

from flask import Blueprint

bp = Blueprint('transportorder', __name__, 
               url_prefix='/transport',
               template_folder='templates',
               static_folder='static')

from . import views