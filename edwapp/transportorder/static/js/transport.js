function changeValueDeleted(){
  var checkbox = document.getElementById('roledeleted');
  if (checkbox.checked != true){ checkbox.value = 0
  }else{ checkbox.value = 1 };
}

function changeValueStackable(){
    var checkbox = document.getElementById('stackable');
    if (checkbox.checked != true){ checkbox.value = 0
    }else{ checkbox.value = 1 };
}

function showMenu() {    
    if (document.getElementById("sideBar").style.width == "15%") {        
        document.getElementById("sideBar").style.width = "0%";
        document.getElementById("mainMenu").style.display = "none";
        document.getElementById("menusvg").style.fill = "black";
    } else {        
        document.getElementById("sideBar").style.width = "15%"
        document.getElementById("mainMenu").style.display = "block";
        document.getElementById("menusvg").style.fill = "white";
        }
}

function getSingleData(ev, s) {
    var getid = ev.target.getAttribute("data-record-id");
    // sometimes when svg clicked do not return A element but use element. this is workaround
    if(getid == null){ var getid = ev.currentTarget.getAttribute("data-record-id"); };
    var gettype = s;
    $.getJSON(
        $SCRIPT_ROOT + '_getJsonData',
        {
            id: getid,
            type: gettype
        },
        function(data){
            var e = JSON.parse(data.res);
            const keys = Object.keys(e);
            var i = 0

            if(gettype == 'user'){
                var inputs = document.getElementById("edituserform").elements;
                for(i; i<keys.length;i++){
                    const subkeys = Object.keys(e[i])
                    for (subkey in subkeys) {
                        // filterout missing input fields for dataset 
                        try{
                            if(subkeys[subkey]=="deleted" || subkeys[subkey]=="active"){
                                inputs[subkeys[subkey]].checked = e[i][subkeys[subkey]];
                            }else{
                                inputs[subkeys[subkey]].value = e[i][subkeys[subkey]];
                            };
                        }catch(err){}
                    };
                };
                if(keys.length){showModal('openModalEditUser','edituserform');}
            };
            if(gettype == 'placeslist'){            
                var inputs = document.getElementById("editplaceform").elements;
                for(i; i<keys.length;i++){
                    const subkeys = Object.keys(e[i])
                    for (subkey in subkeys) {
                        // filterout missing input fields for dataset 
                        try{
                            if(subkeys[subkey]=="countryid"){
                                document.getElementById("edit_country").value = e[i][subkeys[subkey]];
                            }else{
                                inputs["edit_" + subkeys[subkey]].value = e[i][subkeys[subkey]];
                            }
                        
                        }catch(err){}
                    };
                };
                if(keys.length){showModal('openModalEditPlace','editplaceform');};
            };
            if(gettype == 'hauler'){                
                var inputs = document.getElementById("edithaulerform").elements;
                for(i; i<keys.length;i++){
                    const subkeys = Object.keys(e[i])
                    for (subkey in subkeys) {
                        // filterout missing input fields for dataset 
                        try{
                            if(subkeys[subkey]=="countryid"){
                                document.getElementById("edit_country").value = e[i][subkeys[subkey]];
                            }else{
                                inputs["edit_" + subkeys[subkey]].value = e[i][subkeys[subkey]];
                            }
                        
                        }catch(err){}
                    };
                };
                if(keys.length){showModal('openModalEditHauler','edithaulerform');};
            };
            if(gettype == 'contact'){                
                var inputs = document.getElementById("editcontactform").elements;
                for(i; i<keys.length;i++){
                    const subkeys = Object.keys(e[i])
                    for (subkey in subkeys) {
                        // filterout missing input fields for dataset 
                        try{
                        inputs["edit_contact_" + subkeys[subkey]].value = e[i][subkeys[subkey]];
                        }catch(err){}
                    };
                };
                if(keys.length){showModal('openModalEditContact','editcontactform');};
            };
            if(gettype == 'orderhauler'){
                var inputs = document.getElementById("addwinnerform").elements;
                for(i; i<keys.length;i++){
                    const subkeys = Object.keys(e[i])
                    for (subkey in subkeys) {
                        // filterout missing input fields for dataset 
                        try{
                        inputs[('winner' + subkeys[subkey])].value = e[i][subkeys[subkey]];
                        }catch(err){}
                    };
                };
                if(keys.length){showModal('openModalAddWinner','addwinnerform');};
            };
            if(gettype == 'orderapproval'){
                var inputs = document.getElementById("approveorderform").elements;
                for(i; i<keys.length;i++){
                    const subkeys = Object.keys(e[i])
                    for (subkey in subkeys) {
                        // filterout missing input fields for dataset 
                        try{
                        inputs[('approval' + subkeys[subkey])].value = e[i][subkeys[subkey]];
                        }catch(err){}
                    };
                };
                if(keys.length){showModal('openModalApproveOrder','approveorderform');};
            };
            if(gettype == 'orderapprovalbc'){
                var inputs = document.getElementById("approveorderform").elements;
                for(i; i<keys.length;i++){
                    const subkeys = Object.keys(e[i])
                    for (subkey in subkeys) {
                        // filterout missing input fields for dataset 
                        try{
                        inputs[('approval' + subkeys[subkey])].value = e[i][subkeys[subkey]];
                        }catch(err){}
                    };
                };
                if(keys.length){showModal('openModalApproveOrder','approveorderform');};
            };
            if(gettype == 'ordercancel'){
                var inputs = document.getElementById("cancelorderform").elements;
                for(i; i<keys.length;i++){
                    const subkeys = Object.keys(e[i])
                    for (subkey in subkeys) {
                        // filterout missing input fields for dataset 
                        try{
                        inputs[('cancel' + subkeys[subkey])].value = e[i][subkeys[subkey]];
                        }catch(err){}
                    };
                };
                if(keys.length){showModal('openModalCancelOrder','cancelorderform');};
            };
            if(gettype == 'config'){
                var inputs = document.getElementById("changeform").elements;
                for(i; i<keys.length;i++){
                    const subkeys = Object.keys(e[i])
                    for (subkey in subkeys) {
                        // filterout missing input fields for dataset
                        try{
                        inputs[('config' + subkeys[subkey])].value = e[i][subkeys[subkey]];
                        }catch(err){}
                    };
                };
                if(keys.length){showModal('openModalChangeSettings','changeform');};
            };
            if(gettype == 'roles'){
                var inputs = document.getElementById("editrolesform").elements;
                for(i; i<keys.length;i++){
                    const subkeys = Object.keys(e[i])
                    for (subkey in subkeys) {
                        // filterout missing input fields for dataset
                        console.log(('role' + subkeys[subkey]));
                        console.log(e[i][subkeys[subkey]]);
                        try{
                        
                        inputs[('role' + subkeys[subkey])].value = e[i][subkeys[subkey]];
                        }catch(err){}
                    };
                };
                if(keys.length){showModal('openModalEditRoles','editrolesform');};
            };
            if(gettype == 'orderview'){
                document.getElementById('orderstrdata').insertAdjacentHTML( 'beforeend', (e['strdata']));
                if(keys.length){showModal('openModalViewOrder', 'vieworder');};
            };
        }
    );
    return false;
};

function getPlaceData(ev, s) {
    var el = document.getElementById(s + "Place");
    var opt = el.options[el.selectedIndex].value;
    var gettype = 'place';
    $.getJSON(
        $SCRIPT_ROOT + '_getJsonData',
        {
            id: opt,
            type: gettype
        },
        function(data){
            try{
                var e = JSON.parse(data.res);
                const keys = Object.keys(e);
                var i = 0
                //if no choice is maden
                document.getElementById(s + "PlaceInfo").value = "";
                for(i; i<keys.length;i++){
                    if(e[i].name){var str1 = e[i].name + "\n"}else{var str1 = ""};
                    if(e[i].name2){var str2 = e[i].name2+ "\n"}else{var str2 = ""};
                    if(e[i].address1){var str3 = e[i].address1}else{var str3 = ""};
                    if(e[i].streetnumber){var str4 = " " + e[i].streetnumber + "\n"}else{var str4 = "\n"};
                    if(e[i].address2){var str5 = e[i].address2 + "\n"}else{var str5 = ""};
                    if(e[i].area){var str6 = e[i].area + "\n"}else{var str6 = ""};
                    if(e[i].city){var str7 = e[i].city}else{var str7 = ""};
                    if(e[i].zip){var str8 = " " + e[i].zip + "\n"}else{var str8 = "\n"};
                    if(e[i].iso3166alpha3){var str9 = e[i].iso3166alpha3}else{var str9 = ""};
                    if(e[i].continentcode){var str10 = " (continent: " + e[i].continentcode + ")"}else{var str10 = ""};
                    if(e[i].iseu){var str11 = " [European Union]"}else{var str11 = ""};
                    if(e[i].isshengen){var str12 = " [Shengen]"}else{var str12 = ""};
                    var str = str1 + str2 + str3 + str4 + str5 + str6 + str7 + str8 + str9 + str10 + str11 + str12;
                    document.getElementById(s + "PlaceInfo").value = str;
                };           
            }catch(err){document.getElementById(s + "PlaceInfo").value = "";}
        }
    );

    $.getJSON(
        $SCRIPT_ROOT + '_getJsonData',
        {
            id: opt,
            type: 'contacts'
        },
        function(data){
            document.getElementById(s + "ContactInfo").value = "";
            var dynamicSelect = document.getElementById(s+ "Contact");
            // remove contact select options first
            dynamicSelect.value = -1;      
            //remove all options
            while (dynamicSelect.options.length) { dynamicSelect.remove(0); }
            // add empty option
            dynamicSelect.options.add(new Option("", -1), 0);
            
            try{
                var e = JSON.parse(data.res);
                const keys = Object.keys(e);
                for (key in keys)   {
                    // add select options
                    dynamicSelect.add(new Option(opt.innerHTML = e[keys[key]].contactname, e[keys[key]].contactid));
                };
            }catch(err){}    
        }
    );
    return false;
};

function getPlaceContactData(ev, s) {
    var el = document.getElementById(s+ "Contact");
    var opt = el.options[el.selectedIndex].value;
    
    $.getJSON(
        $SCRIPT_ROOT + '_getJsonData',
        {
            id: opt,
            type: 'contact'
        },
        function(data){
            
            try{
                var e = JSON.parse(data.res);
                const keys = Object.keys(e);
                var i = 0
                for(i; i<keys.length;i++){
                    if(e[i].contacttel){var str1 = e[i].contacttel + "   "}else{var str1 = ""};
                    if(e[i].contactemail){var str2 = e[i].contactemail}else{var str2 = ""};
                
                var str = str1 + str2;
                document.getElementById(s+ "ContactInfo").value = str;
                };
            }catch(err){document.getElementById(s + "ContactInfo").value = "";}
        }
    );
    return false;
};


function additem_test(){
    
        var listItem = document.createElement("li");
        var listItemCheckbox = document.createElement("input");
        var listItemLabel = document.createElement("label");
        var editableInput = document.createElement("input");
        var editButton = document.createElement("button");
        var deleteButton = document.createElement("button");
      
        // define types
        listItemCheckbox.type = "checkbox";
        editableInput.type = "text";
      
        // define content and class for buttons
        editButton.innerText = "Edit";
        editButton.className = "edit";
        deleteButton.innerText = "Delete";
        deleteButton.className = "delete";
      
        listItemLabel.innerText = itemText.value;
      
        // appendChild() - append these items to the li
        listElement.appendChild(listItem);
        listItem.appendChild(listItemCheckbox);
        listItem.appendChild(listItemLabel);
        listItem.appendChild(editButton);
        listItem.appendChild(deleteButton);
      
        if (itemText.value.length > 0) {
          itemText.value = "";
          inputFocus(itemText);
        }
}


function additem(serArray){
    // for adding new fields in order items
    var a = serArray
    closeModal('openModalAddItem','additemform');
    var table = document.getElementById("tableofitems");
    var rownumber = table.rows.length-1;
    // Create an empty <tr> element and add it to the 1st position of the table:
    var row = table.insertRow();
    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    var cell0 = row.insertCell(0);
    var cell1 = row.insertCell(1);
    var cell2 = row.insertCell(2);
    var cell3 = row.insertCell(3);
    var cell4 = row.insertCell(4);
    var cell5 = row.insertCell(5);
    var cell6 = row.insertCell(6);
    var cell7 = row.insertCell(7);
    var cell8 = row.insertCell(8);
    // Add some text to the new cells:
    cell0.innerHTML = '<input id="tableofitems-' + rownumber + '-description" name="tableofitems-' + rownumber + '-description" type="text" value="' + a[1].value + '" readonly="true">';
    cell1.innerHTML = '<input id="tableofitems-' + rownumber + '-pieces" name="tableofitems-' + rownumber + '-pieces" type="text" value="' + a[2].value + '" readonly="true">';
    cell2.innerHTML = '<input id="tableofitems-' + rownumber + '-sizewidth" name="tableofitems-' + rownumber + '-sizewidth" type="text" value="' + a[3].value + '" readonly="true">';
    cell3.innerHTML = '<input id="tableofitems-' + rownumber + '-sizelength" name="tableofitems-' + rownumber + '-sizelength" type="text" value="' + a[4].value + '" readonly="true">';
    cell4.innerHTML = '<input id="tableofitems-' + rownumber + '-sizehight" name="tableofitems-' + rownumber + '-sizeheight" type="text" value="' + a[5].value + '" readonly="true">';
    cell5.innerHTML = '<input id="tableofitems-' + rownumber + '-weight" name="tableofitems-' + rownumber + '-weight" type="text" value="' + a[6].value + '" readonly="true">';
    cell6.innerHTML = '<input id="tableofitems-' + rownumber + '-goodsType" name="tableofitems-' + rownumber + '-goodsType" type="text" value="' + a[7].value + '" readonly="true">';
    // when there is stackable NO; a array mises last item and consequent cell is not created as well
    try{
    cell7.innerHTML = '<input id="tableofitems-' + rownumber + '-stackable" name="tableofitems-' + rownumber + '-stackable" type="text" value="' + a[8].value + '" readonly="true">';
    }catch(err){cell7.innerHTML = '<input id="tableofitems-' + rownumber + '-stackable" name="tableofitems-' + rownumber + '-stackable" type="text" value="0" readonly="true">';}
    cell8.innerHTML = '<a onclick="removeitem(this);"><svg><title>Remove</title><use href="#minus"/></svg></a>';
}


function removeitem(r){
    // for removing items from order
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tableofitems").deleteRow(i);    
}


function validate(ev){
    var a = $('#additemform').serializeArray()
    $.getJSON(
        $SCRIPT_ROOT + '_confirmvalidation',
        { 
            dsc: a[1].value,
            pcs: a[2].value,
            wi: a[3].value,
            le: a[4].value,
            he: a[5].value,
            wg: a[6].value,
            tp: a[7].value
        },
        function(data){
            var e = JSON.parse(data.res);            
            closeModal('openModalAddItem','None');
            if(e == 1){
                additem(a);
            }else{
                alert('chybi polozky');
            };
        });
}
