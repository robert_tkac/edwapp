#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from wtforms import (Form, SelectField, StringField, PasswordField, BooleanField, TextAreaField, IntegerField, DecimalField, SubmitField, FieldList, FormField, FloatField, SelectMultipleField)
from wtforms.fields.html5 import EmailField
from wtforms.validators import InputRequired, Length, Email, NoneOf
from edwapp import lazy_gettext
from edwapp.models import Users, TransportPlacesT, GeoCountries
from edwapp.database import db_session


class TransportLoggedUserF(Form):
    userid = IntegerField(lazy_gettext(u'userid'), render_kw={'readonly':'True'})
    name = StringField(lazy_gettext(u'name'), validators=[Length(min=2, message=lazy_gettext(u'errorlength')), ], render_kw={'class':"required"})
    surname = StringField(lazy_gettext(u'lastname'), validators=[Length(min=2, message=lazy_gettext(u'errorlength')), ], render_kw={'class':"required"})
    usertel = StringField(lazy_gettext(u'tel'))
    useremail = EmailField(lazy_gettext(u'email'))
#    roles = SelectField(label=lazy_gettext(u'userroles'), validators=[NoneOf([-1,], message='error'),], coerce=int, 
#        choices=[(-1, lazy_gettext(u'pleasechoose')),] + \
#                [(f.userid, f.roles.rolename) for f in db_session.query(Users).joinfilter(TransportPlacesT.deleted==0).order_by(TransportPlacesT.friendlyname)])
    submit = SubmitField(lazy_gettext(u'submit'), render_kw={'class':'commonSubmit'})


class OrderItemsF(Form):
    description = StringField(lazy_gettext(u'itemdescription'), render_kw={"class": "required modalfirst"})
    pieces = IntegerField(lazy_gettext(u'pieces'), render_kw={"class": "required"})
    sizewidth = StringField(lazy_gettext(u'width_m'), render_kw={"class": "required"})
    sizelength = StringField(lazy_gettext(u'length_m'), render_kw={"class": "required"})
    sizeheight = StringField(lazy_gettext(u'height_m'), render_kw={"class": "required"})
    weight = IntegerField(lazy_gettext(u'weight_kg'), render_kw={"class": "required"})
    goodsType = SelectField(label=lazy_gettext(u'goodsType'), choices=[(-1, lazy_gettext(u'pleasechoose')),
                                                                       ('0', lazy_gettext(u'pallet')),
                                                                       ('1', lazy_gettext(u'box')),
                                                                       ('2', lazy_gettext(u'loose'))], render_kw={"class": "required"})
    stackable = SelectField(label=lazy_gettext(u'stackable'), choices=[('0', lazy_gettext(u'no')),
                                                                       ('1', lazy_gettext(u'yes'))])
    

class DynamicSelectField(SelectField):
    '''https://github.com/wtforms/wtforms/issues/434
    If a form is submitted and the value of a SelectField is an option that was added by Javascript, WTForms will give a 'Not a valid choice' error message.
    This is workaround'''
    def pre_validate(self, form):
        pass


class TransportationWinnerF(Form):
    winnerorderid = IntegerField(label = lazy_gettext(u'orderid'), render_kw = {'readonly':'True'})
    winnerorderTitle = StringField(lazy_gettext(u'ordertitle'), render_kw = {'readonly':'True'})
    winnerEnteredBy = SelectField(label = lazy_gettext(u'winnerenteredby'), coerce=int,
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)
    transportWinner = SelectField(label = lazy_gettext(u'winner'), coerce = int,
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)
    price = FloatField(lazy_gettext(u'winnerprice'))
    currency = SelectField(label = lazy_gettext(u'currency'), coerce = str,
        choices = [])
    looserprice = FloatField(lazy_gettext(u'looserprice'))
    loosercurrency = SelectField(label = lazy_gettext(u'currency'), coerce = str,
        choices = [])
    choiceReason = TextAreaField(label=lazy_gettext(u'choicereasons'))
    winnersubmit = SubmitField(label = lazy_gettext(u'update'), render_kw = {'class':'commonSubmit'})


class TransportOrderCancelationF(Form):
    cancelorderid = IntegerField(lazy_gettext(u'orderid'), render_kw = {'readonly':'True'})
    cancelorderTitle = StringField(lazy_gettext(u'ordertitle'), render_kw = {'readonly':'True'})
    canceledby = SelectField(label=lazy_gettext(u'canceledby'), coerce = int,
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1) 
    cancelationReason = TextAreaField(label=lazy_gettext(u'cancelationReason'))
    cancelsubmit = SubmitField(lazy_gettext(u'update'), render_kw = {'class':'commonSubmit'})


class TransportConfigF(Form):
    configcid = IntegerField(lazy_gettext(u'id'), render_kw = {'readonly':'True'})
    configclabel = StringField(lazy_gettext(u'name'), render_kw = {'readonly':'True'})
    configcvalue = TextAreaField(lazy_gettext(u'value'))
    configcsubmit = SubmitField(lazy_gettext(u'update'), render_kw = {'class':'commonSubmit'})


class TransportOrderApprovalF(Form):
    approvalorderid = IntegerField(lazy_gettext(u'orderid'), render_kw = {'readonly':'True'})
    approvalorderTitle = StringField(lazy_gettext(u'ordertitle'), render_kw = {'readonly':'True'})
    #approvedby = SelectField(label=lazy_gettext(u'approvedby'), coerce = int,
    #    choices = []) 
    approvalsubmit = SubmitField(lazy_gettext(u'approve'), render_kw = {'class':'commonSubmit'})


class TransportOrderF(Form):  
    orderTitle = StringField(lazy_gettext(u'ordertitle'), validators=[Length(min=1, message='toto pole musi byt vyplnene'),], render_kw={"class": "first", 'autofocus': True})
    orderUser = SelectField(label=lazy_gettext(u'orderuser'), validators=[NoneOf([-1,], message='error'),], coerce=int, 
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)
    pickupPlace = SelectField(label=lazy_gettext(u'pickupplace'), validators=[NoneOf([-1,], message='error'),], coerce=int, 
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)
    pickupPlaceInfo = TextAreaField(label=lazy_gettext(u'pickupplaceinfo'), render_kw={'readonly':'True'})
    pickupDate = StringField(lazy_gettext(u'pickupdate'), validators=[Length(min=10, message='toto pole musi byt vyplnene'),], render_kw={"class": "datepicker", 'readonly': True})
    pickupContact = DynamicSelectField(label=lazy_gettext(u'pickupcontact'), choices=[(-1, '')])
    pickupContactInfo = TextAreaField(label=lazy_gettext(u'pickupcontactinfo'), render_kw={'readonly':'True'})
    deliveryPlace = SelectField(label=lazy_gettext(u'deliveryplace'), validators=[NoneOf([-1,], message='error'),], coerce=int, 
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)
    deliveryPlaceInfo = TextAreaField(label=lazy_gettext(u'deliveryplaceinfo'), render_kw={'readonly':'True'})
    deliveryDate = StringField(lazy_gettext(u'deliverydate'), validators=[Length(min=10, message='toto pole musi byt vyplnene'),], render_kw={"class": "datepicker", 'readonly': True})
    deliveryContact = DynamicSelectField(label=lazy_gettext(u'deliverycontact'), choices=[(-1, '')])
    deliveryContactInfo = TextAreaField(label=lazy_gettext(u'deliverycontactinfo'), render_kw={'readonly':'True'})
    tableofitems = FieldList(FormField(OrderItemsF))
    orderNotes = TextAreaField(label=lazy_gettext(u'ordernotes'))
    submit = SubmitField(label=lazy_gettext(u'sendrfq'), render_kw={'class':'commonSubmit'})


class TransportPlacesF(Form):
    edit_placeid = IntegerField(lazy_gettext(u'placeid'), render_kw={'readonly':'True'})
    edit_friendlyname = StringField(lazy_gettext(u'placefriendlyname'), validators=[InputRequired(),])
    edit_name = StringField(lazy_gettext(u'placename'), validators=[InputRequired(),])
    edit_name2 = StringField(lazy_gettext(u'placename2'))
    edit_address1 = StringField(lazy_gettext(u'address1'), validators=[InputRequired(),])
    edit_streetnumber = StringField(lazy_gettext(u'streetnumber'))
    edit_address2 = StringField(lazy_gettext(u'address2'))
    edit_area = StringField(lazy_gettext(u'area'))
    edit_city = StringField(lazy_gettext(u'city'), validators=[InputRequired(),])
    edit_zip = StringField(lazy_gettext(u'zip'), validators=[InputRequired(),])
    edit_country =  SelectField(label=lazy_gettext(u'selectcountry'), coerce=int,
        choices = [])
    edit_deleted = BooleanField(lazy_gettext(u'delete'))
    edit_submit = SubmitField(lazy_gettext(u'update'), render_kw={'class':'commonSubmit'})


class TransportPickHaulerF(Form):
    pick_hauler_friendlyname = SelectField(lazy_gettext(u'placefriendlyname'), coerce = int,
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)
    pick_hauler_hauler = BooleanField(lazy_gettext(u'hauler'))
    pick_hauler_submit = SubmitField(lazy_gettext(u'update'), render_kw={'class':'commonSubmit'})


class TransportAddPlacesF(Form):
    add_friendlyname = StringField(lazy_gettext(u'placefriendlyname'), validators=[InputRequired(),])
    add_name = StringField(lazy_gettext(u'placename'), validators=[InputRequired(),])
    add_name2 = StringField(lazy_gettext(u'placename2'))
    add_address1 = StringField(lazy_gettext(u'address1'), validators=[InputRequired(),])
    add_streetnumber = StringField(lazy_gettext(u'streetnumber'))
    add_address2 = StringField(lazy_gettext(u'address2'))
    add_area = StringField(lazy_gettext(u'area'))
    add_city = StringField(lazy_gettext(u'city'), validators=[InputRequired(),])
    add_zip = StringField(lazy_gettext(u'zip'), validators=[InputRequired(),])
    add_country =  SelectField(label=lazy_gettext(u'selectcountry'), coerce=int,
        choices = [], default = 60)
    add_submit = SubmitField(lazy_gettext(u'add'), render_kw={'class':'commonSubmit'})


class TransportPlacesContactsF(Form):
    edit_contact_contactid = IntegerField(lazy_gettext(u'contactid'), render_kw={'readonly':'True'})
    edit_contact_contactname = StringField(lazy_gettext(u'contactname'))
    edit_contact_contacttel = StringField(lazy_gettext(u'tel'))
    edit_contact_contactemail = StringField(lazy_gettext(u'email'))
    edit_contact_deleted = BooleanField(lazy_gettext(u'delete'))
    edit_contact_submit = SubmitField(lazy_gettext(u'update'), render_kw={'class':'commonSubmit'})


class TransportPlacesAddContactsF(Form):
    add_contact_friendlyname = SelectField(label=lazy_gettext(u'placefriendlyname'), coerce=int, 
        choices = [(-1, lazy_gettext(u'pleasechoose'))], default = -1)
    add_contact_contactname = StringField(lazy_gettext(u'contactname'))
    add_contact_contacttel = StringField(lazy_gettext(u'tel'))
    add_contact_contactemail = StringField(lazy_gettext(u'email'))
    add_contact_submit = SubmitField(lazy_gettext(u'add'), render_kw={'class':'commonSubmit'})


class TransportRolesF(Form):
    roleuserid = IntegerField(lazy_gettext(u'userid'), render_kw = {'readonly':'True'})
    rolename = StringField(lazy_gettext(u'name'), render_kw = {'readonly':'True'})
    rolesurname = StringField(lazy_gettext(u'lastname'), render_kw = {'readonly':'True'})
    roleroles = SelectMultipleField(label=lazy_gettext(u'roles'), coerce = int,
        choices = []) 
    roledeleted = BooleanField(label=lazy_gettext(u'deleted'), render_kw = {'onClick':'changeValueDeleted();'})
    rolesubmit = SubmitField(lazy_gettext(u'update'), render_kw = {'class':'commonSubmit'})