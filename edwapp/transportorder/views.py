# -*- coding: utf-8 -*-

from datetime import datetime
from flask import render_template, redirect, url_for, request, abort, jsonify, json, flash, current_app, g, make_response
from flask_login import current_user
from . import bp
from .forms import (OrderItemsF, TransportLoggedUserF, TransportationWinnerF, TransportPlacesF, TransportOrderF, TransportOrderCancelationF, TransportOrderApprovalF, 
                   TransportPlacesAddContactsF, TransportPlacesContactsF, TransportAddPlacesF, TransportPickHaulerF, TransportConfigF, TransportRolesF)
from edwapp import app, login_manager, lazy_gettext
from edwapp.database import db_session
from edwapp.models import (Users, Roles, association_table, TransportPlacesT, users_01, pickPlaces, delPlaces, TransportConfigT,
                           GeoCountries, GeoContinents, GeoCurrencies, 
                           TransportPlacesContactsT, TransportOrdersT, TransportItemsT)
from edwapp.views import login_required, role_required
from edwapp.smtpemail import sendEmail


#https://pythonise.com/series/learning-flask/flask-message-flashing

@bp.route('/', methods=["POST", "GET"])
def order():
    formorder = TransportOrderF(request.form)
    formitems = OrderItemsF(request.form)
    formorder.orderUser.choices += [(f.userid, f) for f in db_session.query(Users).filter(Users.userid != 1).filter(Users.deleted==0).order_by(Users.surname)]
    formorder.pickupPlace.choices += [(f.placeid, f) for f in db_session.query(TransportPlacesT).filter(TransportPlacesT.deleted==0).order_by(TransportPlacesT.friendlyname)]
    formorder.deliveryPlace.choices += [(f.placeid, f) for f in db_session.query(TransportPlacesT).filter(TransportPlacesT.deleted==0).order_by(TransportPlacesT.friendlyname)]
    
    if request.method == 'POST' and formorder.validate():
        if not addNewOrder(formorder, request.form):
            flash(lazy_gettext(u'somethingwentwrong'), 'error')
        else:
            flash(lazy_gettext(u'ordersaved'), 'info')      
        return redirect(url_for('transportorder.order'))
    g.content['form'] = formorder
    g.content['formitems'] = formitems
    return render_template('order.jinja2', **g.content)


def addNewOrder(objform, req):
    if not objform.tableofitems.entries:
        return False
    order = TransportOrdersT()
    order.orderTitle = objform.orderTitle.data
    order.userid = req.get('orderUser')
    emailcc = [db_session.query(Users).filter(Users.userid==req.get('orderUser')).first().useremail]
    order.pickupPlace = req.get('pickupPlace')    
    order.pickupDate = datetime.strptime(objform.pickupDate.data, '%d-%m-%Y')
    if int(req.get('pickupContact')) > -1:
        order.pickupContactid = req.get('pickupContact')    
    order.deliveryPlace = req.get('deliveryPlace')    
    order.deliveryDate = datetime.strptime(objform.deliveryDate.data, '%d-%m-%Y')    
    if int(req.get('deliveryContact')) > -1:
        order.deliveryContactid = req.get('deliveryContact')    
    order.orderNotes = objform.orderNotes.data
    #order.orderview = json.dumps(orderinfo)
    db_session.add(order)
    db_session.commit()

    oid = order.orderid
    for entry in objform.tableofitems.entries:
        items = TransportItemsT()
        items.orderid = order.orderid
        items.description = entry.data['description']
        items.pieces = entry.data['pieces'] 
        items.sizewidth = entry.data['sizewidth']
        items.sizelength = entry.data['sizelength'] 
        items.sizeheight = entry.data['sizeheight']
        items.weight = entry.data['weight']
        items.goodsType = entry.data['goodsType']
        items.stackable = entry.data['stackable']
        db_session.add(items)
    db_session.commit()

    order.orderview = ordersummary(order.orderid)
    db_session.add(order)
    db_session.commit()
    emailto = getHaulersEmails()
    subject = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_RFQSUBJECT01').first().cvalue).format(oid)
    body = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_RFQBODY01').first().cvalue).format(ordersummary(oid))
    sendEmail(subject, body, emailto, emailcc)
    return True


def getHaulersEmails():
    e = []
    for i in db_session.query(TransportPlacesT).filter(TransportPlacesT.hauler==1).all():
        email = db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.placeid==i.placeid).first()
        if email:
            e += email.contactemail,
    return e


@bp.route('/orderlist')
def orderlist():
    form = TransportationWinnerF(request.form)
    cancelationform = TransportOrderCancelationF(request.form)
    cancelationform.canceledby.choices += [(f.userid, f) for f in db_session.query(Users).filter(Users.userid != 1).filter(Users.deleted == 0).order_by(Users.surname)]
    form.winnerEnteredBy.choices += [(f.userid, f) for f in db_session.query(Users).filter(Users.userid != 1).filter(Users.deleted == 0).order_by(Users.surname)]
    form.transportWinner.choices += [(f.placeid, f) for f in db_session.query(TransportPlacesT).filter(TransportPlacesT.hauler == 1).order_by(TransportPlacesT.friendlyname)]
    form.currency.choices += [(f.currencycode, f.currencycode) for f in db_session.query(GeoCurrencies).order_by(GeoCurrencies.currencycode)]
    form.loosercurrency.choices += [(f.currencycode, f.currencycode) for f in db_session.query(GeoCurrencies).order_by(GeoCurrencies.currencycode)]
    g.content['cancelationform'] = cancelationform
    g.content['winnerform'] = form
    g.content['cancelationform'] = cancelationform
    g.content['rows'] = db_session.query(TransportOrdersT).join(Users, Users.userid == TransportOrdersT.userid).join(users_01, users_01.userid == TransportOrdersT.approvedby1, isouter=True) \
    .filter(TransportOrdersT.orderApprovalDate1 == None) \
    .filter(TransportOrdersT.orderCancelationDate == None)
    return render_template('orderlist.jinja2', **g.content)

@bp.route('/vieworderslist')
def vieworderslist():
    g.content['rows'] = db_session.query(TransportOrdersT).join(Users, Users.userid == TransportOrdersT.userid) \
    .filter((TransportOrdersT.orderApprovalDate1 != None) | (TransportOrdersT.orderApprovalDate2 != None) | (TransportOrdersT.orderCancelationDate != None))
    return render_template('orders.jinja2', **g.content)


@bp.route('/transportconfig', methods=['POST', 'GET'])
@role_required(roles=['admin', 'superuser'])
def transportconfig():
    form = TransportConfigF(request.form)
    g.content['rows'] = db_session.query(TransportConfigT)
    g.content['changeform'] = form
    return render_template('transportconfig.jinja2', **g.content)


@bp.route('/transportconfigchange', methods=['POST', 'GET'])
@role_required(roles=['admin', 'superuser'])
def transportconfigchange():
    form = TransportConfigF(request.form)
    if request.method == 'POST':
        if form.configcvalue.data:
            configtable = TransportConfigT.query.get(form.configcid.data)
            configtable.cvalue = form.configcvalue.data
            db_session.add(configtable)
            db_session.commit()
        else:
            flash(lazy_gettext(u'missingvalue'), 'error')
    return redirect(url_for('transportorder.transportconfig'))


@bp.route('/updatewinner', methods=['POST', 'GET'])
def updatewinner():
    form = TransportationWinnerF(request.form)
    if request.method == 'POST':
        oid = form.winnerorderid.data
        order = TransportOrdersT.query.get(oid)
        
        if int(request.form.get('winnerEnteredBy')) > -1:
            order.winnerEnteredBy = request.form.get('winnerEnteredBy')
        else:
            flash(lazy_gettext(u'missingwhoentered'), 'error')
        if int(request.form.get('transportWinner')) > -1:
            order.transportWinner = request.form.get('transportWinner')
        else:
            flash(lazy_gettext(u'missinghauler'), 'error')
        if form.price.data:
            if float(form.price.data) < 0:
                flash (lazy_gettext(u'negativeprice'), 'error')
            elif int(db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='MAXORDERPRICE').first().cvalue) > form.price.data:
                order.price = form.price.data
            else:
                flash (lazy_gettext(u'toobigprice'), 'error')
        else:
            flash (lazy_gettext(u'missingprice'), 'error')
        if request.form.get('currency'):
            order.currency = request.form.get('currency')
        else:
            flash(lazy_gettext(u'missingcurrency'), 'error')
        if form.looserprice.data:
            order.looserprice = form.looserprice.data
        if request.form.get('loosercurrency'):
            order.loosercurrency = request.form.get('loosercurrency')

        if order.winnerEnteredBy and order.transportWinner and order.price and order.currency:
            order.winnerEnetredDate = datetime.now()
            db_session.add(order)
            db_session.commit()
            # autoapproval
            #if price is in level 1 approval and userrole of user order is mc auto approve and send email
            currentOrder = db_session.query(TransportOrdersT).filter(TransportOrdersT.orderid == oid).first()
            l1price = int(db_session.query(TransportConfigT).filter(TransportConfigT.clabel == 'PRICE2LEVEL').first().cvalue)
            orderUserRoles = db_session.query(Users).filter(Users.userid == currentOrder.userid).first().roles
            if l1price > currentOrder.price:
                for role in orderUserRoles:
                    if 'mc' == str(role):
                        #send order and add default approver
                        defaultApprover = db_session.query(Users).join(association_table).join(Roles).filter(Roles.rolename == 'defaultapprover').first().userid
                        order.approvedby1 = defaultApprover
                        order.orderApprovalDate1 = datetime.now()
                        db_session.add(order)
                        db_session.commit()
                        # send emails
                        emailcc = [db_session.query(Users).filter(Users.userid==(db_session.query(TransportOrdersT).filter(TransportOrdersT.orderid==oid).first().userid)).first().useremail]
                        emailto = [db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.placeid == order.transportWinner).first().contactemail]
                        subject = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_ORDSUBJECT01').first().cvalue).format(oid)
                        body = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_ORDBODY01').first().cvalue).format(ordersummary(oid))            
                        sendEmail(subject, body, emailto, emailcc)
                        
                        # looser email
                        loosersubject = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_LOOSERSUBJECT01').first().cvalue).format(oid)
                        looserbody = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_LOOSERBODY01').first().cvalue).format(ordersummary(oid))
                        
                        looseremail = []
                        for lid in db_session.query(TransportPlacesT).filter(TransportPlacesT.hauler == 1).filter(TransportPlacesT.placeid != order.transportWinner).all():
                            looseremail += db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.placeid == lid.placeid).first().contactemail,
        
                        sendEmail(loosersubject, looserbody, looseremail)
            # send notification to approver1 when not autoapproved
            else:
                approveremailto = [(f.useremail) for f in db_session.query(Users).join(association_table).join(Roles).filter(Roles.rolename == 'approver1email')]
                approversubject = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_APPROVERSUBJECT01').first().cvalue)
                approverbody = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_APPROVERBODY01').first().cvalue)
                sendEmail(approversubject, approverbody, approveremailto)

    return redirect(url_for('transportorder.orderlist'))


@bp.route('/approveorderlist')
@role_required(roles=['admin', 'approver1', 'superuser'])
def approveorderlist():
    form = TransportOrderApprovalF(request.form)
    #form.approvedby.choices += [(f.userid, f) for f in db_session.query(Users).filter(Users.userid != 1).filter(Users.deleted == 0).order_by(Users.surname)]
    cancelationform = TransportOrderCancelationF(request.form)
    cancelationform.canceledby.choices += [(f.userid, f) for f in db_session.query(Users).filter(Users.userid != 1).filter(Users.deleted == 0).order_by(Users.surname)]
    g.content['approvalform'] = form
    g.content['cancelationform'] = cancelationform
    g.content['rows'] = db_session.query(TransportOrdersT).join(Users, Users.userid==TransportOrdersT.userid).join(users_01, users_01.userid==TransportOrdersT.approvedby1, isouter=True) \
    .filter(TransportOrdersT.winnerEnetredDate!=None) \
    .filter(TransportOrdersT.orderApprovalDate1==None) \
    .filter(TransportOrdersT.orderCancelationDate==None)
    return render_template('approvallist.jinja2', **g.content)


@bp.route('/approveorderlistbc')
@role_required(roles=['admin', 'approver2', 'superuser'])
def approveorderlistbc():
    level2price = int(db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='PRICE2LEVEL').first().cvalue)
    form = TransportOrderApprovalF(request.form)
    #form.approvedby.choices += [(f.userid, f) for f in db_session.query(Users).filter(Users.userid != 1).filter(Users.deleted == 0).order_by(Users.surname)]
    cancelationform = TransportOrderCancelationF(request.form)
    cancelationform.canceledby.choices += [(f.userid, f) for f in db_session.query(Users).filter(Users.userid != 1).filter(Users.deleted == 0).order_by(Users.surname)]
    g.content['approvalform'] = form
    g.content['cancelationform'] = cancelationform
    g.content['rows'] = db_session.query(TransportOrdersT).join(Users, Users.userid==TransportOrdersT.userid).join(users_01, users_01.userid==TransportOrdersT.approvedby2, isouter=True) \
    .filter(TransportOrdersT.winnerEnetredDate!=None) \
    .filter(TransportOrdersT.orderApprovalDate1!=None) \
    .filter(TransportOrdersT.orderApprovalDate2==None) \
    .filter(TransportOrdersT.orderCancelationDate==None) \
    .filter(TransportOrdersT.price>=level2price)
    return render_template('approvallistbc.jinja2', **g.content)

@bp.route('/tst', methods=['GET', "POST"])
@role_required(roles=['admin'])
def tst():
    pass
    #print( [f.useremail for f in db_session.query(Users).join(association_table).join(Roles).filter(Roles.rolename.in_(['approver1', 'approver2']))] )
    return redirect(url_for('transportorder.orderlist'))

@bp.route('/manual')
def manual():
    return render_template('manual.jinja2', **g.content)


@bp.route('/approveorder', methods=['GET', "POST"])
@role_required(roles=['admin', 'approver1', 'superuser'])
def approveorder():
    form = TransportOrderApprovalF(request.form)
    # this is bypassed when autoapproval is in place (price less then level1 and user role is MC)
    if request.method == 'POST':
        oid = form.approvalorderid.data
        order = TransportOrdersT.query.get(oid)
        #if request.form.get('approvedby'):
        #    order.approvedby = request.form.get('approvedby')
        #else:
        #    flash(flash(lazy_gettext(u'missingapprover'), 'error'))
        #    return redirect(url_for('transportorder.approveorderlist'))
        order.approvedby1 = current_user.userid
        order.orderApprovalDate1 = datetime.now()
        db_session.add(order)
        db_session.commit()
        orderPrice = db_session.query(TransportOrdersT).filter(TransportOrdersT.orderid==form.approvalorderid.data).first().price
        cclevel = int(db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='PRICE2LEVEL').first().cvalue)
        if cclevel > orderPrice:
            emailcc = [db_session.query(Users).filter(Users.userid==(db_session.query(TransportOrdersT).filter(TransportOrdersT.orderid==oid).first().userid)).first().useremail]
            emailto = [db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.placeid == order.transportWinner).first().contactemail]
            subject = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_ORDSUBJECT01').first().cvalue).format(oid)
            body = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_ORDBODY01').first().cvalue).format(ordersummary(oid))            
            sendEmail(subject, body, emailto, emailcc)
            # looser email
            loosersubject = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_LOOSERSUBJECT01').first().cvalue).format(oid)
            looserbody = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_LOOSERBODY01').first().cvalue).format(ordersummary(oid))
            
            looseremail = []
            for lid in db_session.query(TransportPlacesT).filter(TransportPlacesT.hauler == 1).filter(TransportPlacesT.placeid != order.transportWinner).all():
                looseremail += db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.placeid == lid.placeid).first().contactemail,
            
            sendEmail(loosersubject, looserbody, looseremail)
        # when prive is higher then level1 send notification to approver 2        
        else:
            approveremailto = [(f.useremail) for f in db_session.query(Users).join(association_table).join(Roles).filter(Roles.rolename == 'approver2email')]
            approversubject = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_APPROVERSUBJECT01').first().cvalue)
            approverbody = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_APPROVERBODY01').first().cvalue)
            sendEmail(approversubject, approverbody, approveremailto)

    return redirect(url_for('transportorder.approveorderlist'))


@bp.route('/approveorderbc', methods=['GET', "POST"])
@role_required(roles=['admin', 'approver2', 'superuser'])
def approveorderbc():
    form = TransportOrderApprovalF(request.form)
    if request.method == 'POST':
        oid = form.approvalorderid.data
        order = TransportOrdersT.query.get(oid)
        #if request.form.get('approvedby'):
        #    order.approvedby = request.form.get('approvedby')
        #else:
        #    flash(flash(lazy_gettext(u'missingapprover'), 'error'))
        #    return redirect(url_for('transportorder.approveorderlist'))
        order.approvedby2 = current_user.userid
        order.orderApprovalDate2 = datetime.now()
        db_session.add(order)
        db_session.commit()
        emailcc = [db_session.query(Users).filter(Users.userid==(db_session.query(TransportOrdersT).filter(TransportOrdersT.orderid==oid).first().userid)).first().useremail]
        emailto = [db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.placeid == order.transportWinner).first().contactemail]
        subject = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_ORDSUBJECT01').first().cvalue).format(oid)
        body = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_ORDBODY01').first().cvalue).format(ordersummary(oid))
        sendEmail(subject, body, emailto, emailcc)
        # looser email
        loosersubject = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_LOOSERSUBJECT01').first().cvalue).format(oid)
        looserbody = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_LOOSERBODY01').first().cvalue).format(ordersummary(oid))

        looseremail = []
        for lid in db_session.query(TransportPlacesT).filter(TransportPlacesT.hauler == 1).filter(TransportPlacesT.placeid != order.transportWinner).all():
            looseremail += db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.placeid == lid.placeid).first().contactemail,
        
        sendEmail(loosersubject, looserbody, looseremail)
    return redirect(url_for('transportorder.approveorderlistbc'))


@bp.route('/cancelorder', methods=['GET', "POST"])
def cancelorder():
    form = TransportOrderCancelationF(request.form)
    if request.method == 'POST':
        oid = form.cancelorderid.data
        order = TransportOrdersT.query.get(oid)
        if request.form.get('canceledby'):
            order.canceledby = request.form.get('canceledby')
        else:
            flash(lazy_gettext(u'missingcanceledby'), 'error')
        if form.cancelationReason.data:
            order.cancelationReason = form.cancelationReason.data
        else:
            flash(lazy_gettext(u'missingcancelationreason'), 'error')
        if order.canceledby and order.cancelationReason:
            order.orderCancelationDate = datetime.now()
            db_session.add(order)
            db_session.commit()
            userordered = db_session.query(Users).filter(Users.userid==(db_session.query(TransportOrdersT).filter(TransportOrdersT.orderid==oid).first().userid)).first()
            emailcc = userordered.useremail
            emailto = getHaulersEmails()
            subject = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_CANCELSUBJECT01').first().cvalue).format(oid)
            body = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_CANCELBODY01').first().cvalue).format(oid, form.cancelationReason.data)
            sendEmail(subject, body, emailto, emailcc)
    return redirect(request.args.get('next') or request.referrer or url_for('index'))


@bp.route('/userlist', methods=['GET', "POST"])
def userlist():
    form = TransportLoggedUserF(request.form)
    if request.method == 'POST' and form.validate():
        usr = Users.query.get(form.userid.data)
        usr.name = form.name.data
        usr.surname = form.surname.data
        usr.usertel = form.usertel.data
        usr.useremail = form.useremail.data
        db_session.add(usr)
        db_session.commit()

    g.content['form'] = form
    g.content['rows'] = db_session.query(Users).join(association_table, isouter=True).join(Roles, isouter=True).filter(Users.deleted==0).order_by(Users.surname)
    return render_template('userlist.jinja2', **g.content)


@bp.route('/contactlist', methods=['GET', "POST"])
def contactlist():
    addcontactform = TransportPlacesAddContactsF(request.form)
    editcontactform = TransportPlacesContactsF(request.form)
    rows = db_session.query(TransportPlacesContactsT).join(TransportPlacesT, TransportPlacesContactsT.placeid == TransportPlacesT.placeid) \
                                .filter(TransportPlacesContactsT.deleted == 0) \
                                .filter(TransportPlacesT.deleted == 0).order_by(TransportPlacesT.friendlyname)
    addcontactform.add_contact_friendlyname.choices += [(f.placeid, f) for f in db_session.query(TransportPlacesT).filter(TransportPlacesT.deleted == 0).order_by(TransportPlacesT.friendlyname)]
    g.content['rows'] = rows
    g.content['addcontactform'] = addcontactform
    g.content['editcontactform'] = editcontactform
    return render_template('contactlist.jinja2', **g.content)


@bp.route('/roleslist', methods=['GET', "POST"])
@role_required(roles=['admin', 'superuser'])
def roleslist():
    rolesform = TransportRolesF(request.form)
    rolesform.roleroles.choices += [(f.roleid, f) for f in db_session.query(Roles).filter(Roles.rolearea == 'logistics').order_by(Roles.rolename)]
    rows = db_session.query(Users).join(association_table, isouter=True).join(Roles, isouter=True).filter(Users.userid!=1).filter(Users.deleted==False).order_by(Users.surname)
    g.content['rows'] = rows
    g.content['rolesform'] = rolesform
    return render_template('roleslist.jinja2', **g.content)


@bp.route('/changerole', methods=['GET', "POST"])
@role_required(roles=['admin', 'superuser'])
def changerole():
    form = TransportRolesF(request.form)
    if request.method == 'POST':
        user = Users.query.get(form.roleuserid.data)
        user.deleted = form.roledeleted.data
        #delete logistics roles first
        for i in db_session.query(Roles).filter(Roles.rolearea=='logistics').all():
            try:
                user.roles.remove(i)
            except:
                pass
        for i in form.roleroles.data:
            user.roles.append(Roles.query.filter(Roles.roleid==i).one())
        db_session.add(user)
        db_session.commit()
    return redirect(url_for('transportorder.roleslist'))


@bp.route('/addcontact', methods=['GET', "POST"])
def addcontact():
    form = TransportPlacesAddContactsF(request.form)
    if request.method == 'POST':
        contact = TransportPlacesContactsT()
        if request.form.get('add_contact_friendlyname') != '-1':
            contact.placeid = request.form.get('add_contact_friendlyname')
        else:
            flash(lazy_gettext(u'missingplace'), 'error')
        contact.contactname = form.add_contact_contactname.data
        contact.contacttel = form.add_contact_contacttel.data
        contact.contactemail = form.add_contact_contactemail.data
        if form.add_contact_contactname.data or form.add_contact_contacttel.data or form.add_contact_contactemail.data:
            db_session.add(contact)
            db_session.commit()
            flash(lazy_gettext(u'done'), 'info')
        else:
            flash(lazy_gettext(u'missingoneofthree'), 'error')
    return redirect(url_for('transportorder.contactlist'))


@bp.route('/editcontact', methods=['GET', "POST"])
def editcontact():
    form = TransportPlacesContactsF(request.form)
    if request.method == 'POST':
        contact = TransportPlacesContactsT.query.get(form.edit_contact_contactid.data)
        contact.contactname = form.edit_contact_contactname.data
        contact.contacttel = form.edit_contact_contacttel.data
        contact.contactemail = form.edit_contact_contactemail.data
        contact.deleted = form.edit_contact_deleted.data
        if form.edit_contact_deleted.data or form.edit_contact_contactname.data or form.edit_contact_contacttel.data or form.edit_contact_contactemail.data:
            db_session.add(contact)
            db_session.commit()
            flash(lazy_gettext(u'updated'), 'info')
        else:
            flash(lazy_gettext(u'missingoneofthree'), 'error')

    return redirect(url_for('transportorder.contactlist'))

def ordersummary(orderid):
    '''
    this function creates html string for email and order overview
    '''
    vorder = db_session.query(TransportOrdersT).join(Users, Users.userid == TransportOrdersT.userid). \
                                filter(TransportOrdersT.orderid == orderid).one()

    vpick = db_session.query(TransportPlacesT).join(GeoCountries, GeoCountries.countryid == TransportPlacesT.countryid). \
                                filter(TransportPlacesT.placeid == vorder.pickupPlace).one()

    vpickcont = db_session.query(TransportPlacesContactsT). \
                                filter(TransportPlacesContactsT.contactid == vorder.pickupContactid).first()

    vdeli = db_session.query(TransportPlacesT).join(GeoCountries, GeoCountries.countryid == TransportPlacesT.countryid). \
                                filter(TransportPlacesT.placeid == vorder.deliveryPlace).one()

    vdelicont = db_session.query(TransportPlacesContactsT). \
                                filter(TransportPlacesContactsT.contactid == vorder.deliveryContactid).first()

    vitems = db_session.query(TransportItemsT).filter(TransportItemsT.orderid == orderid).all()
    pckgTypes = {0 : lazy_gettext(u'pallet'), 1 : lazy_gettext(u'box'), 2 : lazy_gettext(u'loose')}
    stritems = ''
    for item in vitems:
        stritems += '''<tr>
<td style="border:1px solid black;border-collapse:collapse;padding:5px;">{0}</td>
<td style="border:1px solid black;border-collapse:collapse;padding:5px;">{1}</td>
<td style="border:1px solid black;border-collapse:collapse;padding:5px;">{2}x{3}x{4}</td>
<td style="border:1px solid black;border-collapse:collapse;padding:5px;">{5}</td>
<td style="border:1px solid black;border-collapse:collapse;padding:5px;">{6}</td>
<td style="border:1px solid black;border-collapse:collapse;padding:5px;">{7}</td></tr>'''.format(
            item.description,
            item.pieces,
            item.sizewidth,
            item.sizeheight,
            item.sizelength,
            item.weight,
            pckgTypes[item.goodsType],
            lazy_gettext(u'yes') if item.stackable else lazy_gettext(u'no'))

    resultstr = '''<dl><dt style="font-weight:bold;">{34}:</dt><dd style="padding-bottom:8px;">{0}</dd>
<dt style="font-weight:bold;">{35}:</dt><dd style="padding-bottom:8px;">{1}</dd>
<dt style="font-weight:bold;">{36}:</dt><dd style="padding-bottom:8px;">{2} {3}<br/>{4}email: {5}</dd>
<dt style="font-weight:bold;">{37}:</dt><dd style="padding-bottom:8px;">{6}</dd>
<dt style="font-weight:bold;">{38}:</dt><dd style="padding-bottom:8px;">{7}<br/>{8}{9} {10}<br/>{11}{12}{13} {14}<br/>{15}</dd>
<dt style="font-weight:bold;">{39}:</dt><dd style="padding-bottom:8px;">{16}{17}{18}</dd>
<dt style="font-weight:bold;">{40}:</dt><dd style="padding-bottom:8px;">{19}</dd>
<dt style="font-weight:bold;">{41}:</dt><dd style="padding-bottom:8px;">{20}<br/>{21}{22} {23}<br/>{24}{25}{26} {27}<br/>{28}</dd>
<dt style="font-weight:bold;">{42}:</dt><dd style="padding-bottom:8px;">{29}{30}{31}</dd>
<dt style="font-weight:bold;">{43}:</dt><dd style="padding-bottom:8px;">{32}</dd></dl>
<table id="viewtable" style="border:1px solid black;border-collapse:collapse;">
<caption style="text-align:left;">{44}:</caption>
<tr><th style="border:1px solid black;border-collapse:collapse;padding:5px;">{45}</th>
<th style="border:1px solid black;border-collapse:collapse;padding:5px;">{46}</th>
<th style="border:1px solid black;border-collapse:collapse;padding:5px;">{47}</th>
<th style="border:1px solid black;border-collapse:collapse;padding:5px;">{48}</th>
<th style="border:1px solid black;border-collapse:collapse;padding:5px;">{49}</th>
<th style="border:1px solid black;border-collapse:collapse;padding:5px;">{50}</th></tr>
{33}
</table>'''. \
                    format(vorder.orderid,
                           vorder.orderTitle,  
                           vorder.users.name, 
                           vorder.users.surname,
                           ('tel:' + vorder.users.usertel + '<br/>') if vorder.users.usertel else '',
                           vorder.users.useremail,
                           vorder.pickupDate.strftime('%d. %m. %Y'),
                           vpick.name,
                           (vpick.name2 + '<br/>') if vpick.name2 else '',
                           vpick.address1,
                           vpick.streetnumber, # 10
                           (vpick.address2 + '<br/>') if vpick.address2 else '',
                           (vpick.area + '<br/>') if vpick.area else '',
                           vpick.zip,
                           vpick.city,
                           vpick.countries.iso3166alpha2,
                           ((vpickcont.contactname + '<br/>') if vpickcont.contactname else '') if vpickcont else '',
                           (('tel: ' + vpickcont.contacttel + '<br/>') if vpickcont.contacttel else '') if vpickcont else '',
                           (('email: ' + vpickcont.contactemail + '<br/>') if vpickcont.contactemail else '') if vpickcont else '',
                           vorder.deliveryDate.strftime('%d. %m. %Y'),
                           vdeli.name, 
                           (vdeli.name2 + '<br/>') if vdeli.name2 else '',
                           vdeli.address1,
                           vdeli.streetnumber, # 10
                           (vdeli.address2 + '<br/>') if vdeli.address2 else '',
                           (vdeli.area + '<br/>') if vdeli.area else '',
                           vdeli.zip,
                           vdeli.city,
                           vdeli.countries.iso3166alpha2,
                           ((vdelicont.contactname + '<br/>') if vdelicont.contactname else '') if vdelicont else '',
                           (('tel: ' + vdelicont.contacttel + '<br/>') if vdelicont.contacttel else '') if vdelicont else '',
                           (('email: ' + vdelicont.contactemail + '<br/>') if vdelicont.contactemail else '') if vdelicont else '',
                           vorder.orderNotes,
                           stritems,
                           lazy_gettext(u'orderid'),
                           lazy_gettext(u'ordertitle'),
                           lazy_gettext(u'orderuser'),
                           lazy_gettext(u'pickupdate'),
                           lazy_gettext(u'pickupplace'),
                           lazy_gettext(u'pickupcontact'),
                           lazy_gettext(u'deliverydate'),
                           lazy_gettext(u'deliveryplace'),
                           lazy_gettext(u'deliverycontact'),
                           lazy_gettext(u'ordernotes'),
                           lazy_gettext(u'transportitems'),
                           lazy_gettext(u'itemdescription'),
                           lazy_gettext(u'pieces'),
                           lazy_gettext(u'whd_m'),
                           lazy_gettext(u'weight_kg'),
                           lazy_gettext(u'goodsType'),
                           lazy_gettext(u'stackable'))

    return resultstr


def getJsonObj(data):
    '''
    convert ORM query object to to dictionary, remove instance, dump to json object,
    which can be passed to jsonify method as ORM object cannot be directly serialized
    '''
    dct = {}
    innerdct = {}
    for e, record in enumerate(data):
        for attr, value in record.__dict__.items():
            if attr in ('deliveryDate', 'pickupDate'):
                innerdct[attr] = value.strftime('%m-%d-%Y')
            else:
                innerdct[attr] = value
        innerdct.pop('_sa_instance_state', None)
        try:
            for attr, value in record.countries.__dict__.items():
                innerdct[attr] = value
            innerdct.pop('_sa_instance_state', None)
        except:
            pass
        try:
            for attr, value in record.users.__dict__.items():
                innerdct[attr] = value
            innerdct.pop('_sa_instance_state', None)
        except:
            pass
        try:
            for attr, value in record.pickplaces.__dict__.items():
                innerdct['pick_' + attr] = value
            innerdct.pop('pick__sa_instance_state', None)
        except:
            pass
        try:
            for attr, value in record.deliveryplaces.__dict__.items():                
                innerdct['del_' + attr] = value
            innerdct.pop('del__sa_instance_state', None)
        except:
            pass
        try:
            for attr, value in record.country.__dict__.items():                
                innerdct['del_' + attr] = value
            innerdct.pop('del__sa_instance_state', None)
        except:
            pass
        dct[e] = innerdct
        innerdct = {}
    return json.dumps(dct)


@bp.route('/_getJsonData', methods=['GET',])
def getJsonData():
    '''
    receive clicked object ID and type from js and return json data object back
    '''
    rID = request.args.get('id', 1, type=str)
    rType = request.args.get('type', 2, type=str)
    # option is blank option
    if rID in (-1, '-1'):
        return jsonify(res="")

    if rType == 'user':
        returnData = db_session.query(Users).filter(Users.userid == rID).filter(Users.deleted == 0).filter(Users.userid != 1)
    elif rType == 'place':
        returnData = db_session.query(TransportPlacesT).join(GeoCountries).filter(TransportPlacesT.placeid == rID).filter(TransportPlacesT.deleted == 0)
    elif rType == 'placeslist':
        returnData = db_session.query(TransportPlacesT).join(GeoCountries).filter(TransportPlacesT.placeid == rID).filter(TransportPlacesT.deleted == 0).filter(TransportPlacesT.hauler != 1)
    elif rType == 'contacts':
        returnData = db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.placeid == rID).filter(TransportPlacesContactsT.deleted == 0)
    elif rType == 'contact':
        #exclude possibility to edit haulers contacts
        haulers = [(f.placeid) for f in db_session.query(TransportPlacesT).filter(TransportPlacesT.hauler == 1)]
        returnData = db_session.query(TransportPlacesContactsT).filter(TransportPlacesContactsT.contactid == rID).filter(TransportPlacesContactsT.deleted == 0).filter(TransportPlacesContactsT.placeid.notin_(haulers))
    elif rType == 'config':
        returnData = db_session.query(TransportConfigT).filter(TransportConfigT.cid == rID)
    elif rType == 'roles':
        returnData = db_session.query(Users).filter(Users.userid == rID)
    elif rType in ('orderhauler','orderapproval', 'orderapprovalbc', 'ordercancel'):
        returnData = db_session.query(TransportOrdersT).filter(TransportOrdersT.orderid == rID)
    elif rType == 'orderview':
        returnDataView = json.dumps({'strdata' : ordersummary(rID)})
    elif rType == 'hauler':
        returnData = db_session.query(TransportPlacesT).filter(TransportPlacesT.deleted == 0).filter(TransportPlacesT.hauler == 1)
    # bypass serialisation from ORM object
    if rType == 'orderview':
        j = returnDataView
    else:
        j = getJsonObj(returnData)
    return jsonify(res=j)


@bp.route('/_confirmvalidation', methods=['POST', 'GET'])
def confirmValidation():
    
    errmessage = json.dumps({'msg':'wrongentryaddagain'})
    if not request.args.get('dsc'):
        return jsonify(res='0')
    try:
        if request.args.get('pcs', type=int) < 1:
            return jsonify(res=errmessage)
        if request.args.get('wi', type=int) < 1:
            return jsonify(res=errmessage)
        if request.args.get('le', type=int) < 1:
            return jsonify(res=errmessage)
        if request.args.get('he', type=int) < 1:
            return jsonify(res=errmessage)
        if request.args.get('wg', type=int) < 1:
            return jsonify(res=errmessage)
    except:
        return jsonify(res=errmessage)
    
    if request.args.get('tp') == -1:
        return jsonify(res=errmessage)
    # success in form
    return jsonify(res='1')


@bp.route('/places', methods=['GET',])
def places():
    form = TransportPlacesF(request.form)
    addplaceform = TransportAddPlacesF(request.form)
    form.edit_country.choices += [(f.countryid, f) for f in db_session.query(GeoCountries).order_by(GeoCountries.countryname)]
    addplaceform.add_country.choices += [(f.countryid, f) for f in db_session.query(GeoCountries).order_by(GeoCountries.countryname)]
    g.content['form'] = form
    g.content['addplaceform'] = addplaceform
    g.content['rows'] = db_session.query(TransportPlacesT).join(GeoCountries).join(GeoContinents).order_by(TransportPlacesT.friendlyname)
    return render_template('places.jinja2', **g.content)


@bp.route('/addplace', methods=['POST',])
def addplace():
    form = TransportAddPlacesF(request.form)
    if request.method == 'POST':
        place = TransportPlacesT()
        place.friendlyname = form.add_friendlyname.data
        place.name = form.add_name.data
        place.name2 = form.add_name2.data
        place.address1 = form.add_address1.data
        place.streetnumber =  form.add_streetnumber.data
        place.address2 = form.add_address2.data
        place.area = form.add_area.data
        place.city = form.add_city.data
        place.zip = form.add_zip.data
        place.countryid = request.form.get('add_country')
        try:
            db_session.add(place)
            db_session.commit()
            flash(lazy_gettext(u'added'), 'info')
        except:
            flash(lazy_gettext(u'itemalreadyexists'), 'error')
    return redirect(url_for('transportorder.places'))


@bp.route('/updateplace', methods=['POST',])
def updateplace():
    form = TransportPlacesF(request.form)
    if request.method == 'POST':
        place = TransportPlacesT.query.get(form.edit_placeid.data)
        place.friendlyname = form.edit_friendlyname.data
        place.name = form.edit_name.data
        place.name2 = form.edit_name2.data
        place.address1 = form.edit_address1.data
        place.streetnumber =  form.edit_streetnumber.data
        place.address2 = form.edit_address2.data
        place.area = form.edit_area.data
        place.city = form.edit_city.data
        place.zip = form.edit_zip.data
        place.countryid = request.form.get('edit_country')
        try:
            db_session.add(place)
            db_session.commit()
            flash(lazy_gettext(u'added'), 'info')
        except:
            flash(lazy_gettext(u'itemalreadyexists'), 'error')
    return redirect(url_for('transportorder.places'))


@bp.route('/haulerslist', methods=['POST', 'GET'])
def haulerslist():
    form = TransportPickHaulerF(request.form)
    form.pick_hauler_friendlyname.choices += [(f.placeid, f) for f in db_session.query(TransportPlacesT).order_by(TransportPlacesT.friendlyname)]
    g.content['form'] = form
    g.content['rows'] = db_session.query(TransportPlacesT).filter(TransportPlacesT.hauler == 1)
    return render_template('haulerslist.jinja2', **g.content)


@bp.route('/pickhaulers', methods=['POST',])
@role_required(roles=['admin', 'approver', 'superuser'])
def pickhaulers():
    form = TransportPickHaulerF(request.form)
    if request.method == 'POST':
        hauler = TransportPlacesT.query.get(request.form.get('pick_hauler_friendlyname'))
        try:
            hauler.hauler = form.pick_hauler_hauler.data
            db_session.add(hauler)
            db_session.commit()
            flash(lazy_gettext(u'updated'), 'info')
        except:
            flash(lazy_gettext(u'chooseplace'), 'error')
    return redirect(url_for('transportorder.haulerslist'))
