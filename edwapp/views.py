# -*- coding: utf-8 -*-
import functools, os
from flask import render_template, request, current_app, flash, redirect, url_for, abort, g, session, send_from_directory
from flask_login import login_required, logout_user, current_user, login_user
from urllib.parse import urlparse, urljoin
from . import app, babel, login_manager, lazy_gettext
from .models import Users, Roles
from .forms import LoginF, PasswordChangeF, AddUserF
from .database import db_session


#pass modal forms in layout.jinja2 template to enable field parsing
@app.before_request
def before_request_func():
    '''create global dictionary. every render_tmplate will apply it **g.content and can add its own forms as well
       and clear flashe messages   
    '''
    #session.pop('_flashes', None)
    g.content = {
        'loginform': LoginF(request.form),
        'sessionlanguage': session.get('language', request.accept_languages.best_match(app.config['LANGUAGES'].keys()))
        }

@babel.localeselector
def get_locale():
    if request.args.get('lang'):
        session['language'] = request.args.get('lang')
        #babel.refresh()
        return request.args.get('lang')
    lng = session.get('language', app.config['BABEL_DEFAULT_LOCALE'])
    return lng #request.accept_languages.best_match(app.config['LANGUAGES'].keys())

@app.template_filter()
def format_datetime(value, format='medium'):
    '''https://stackoverflow.com/questions/4830535/how-do-i-format-a-date-in-jinja2'''
    if format == 'full':
        format="EEEE, d. MMMM y 'at' HH:mm"
    elif format == 'medium':
        format="EE dd.MM.y HH:mm"
    return babel.dates.format_datetime(value, format)

def role_required(roles=[]):
    '''
    https://stackoverflow.com/questions/15871391/implementing-flask-login-with-multiple-user-classes
    '''
    def wrapper(fn):
        @functools.wraps(fn)
        def decorated_view(*args, **kwargs):
            if not current_user.is_authenticated:
              return login_manager.unauthorized()
            
            notfound = True
            uroles = [i.rolename for i in current_user.roles]
            for role in roles:
                if (role in uroles):
                    notfound = False
                    continue
            if notfound:        
                return abort(401)
                #return unauthorized()
            return fn(*args, **kwargs)
        return decorated_view
    return wrapper

def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
           ref_url.netloc == test_url.netloc

def get_redirect_target():
    for target in request.args.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target

def redirect_url(default='index'):
    return request.args.get('next') or request.referrer or url_for(default)

@login_manager.user_loader
def load_user(user_id):
    """Check if user is logged-in on every page load."""
    if user_id is not None:
        return Users.query.get(user_id)
    return None

#@login_manager.unauthorized_handler
#def unauthorized():
#    """Redirect unauthorized users to Login page."""
#    #flash('You must be logged in to view that page.', 'warning')
#    return render_template('403.jinja2', **g.content), 403

@app.route("/login", methods=["GET", "POST"])
def login():
    """For GET requests, display the login form. 
    For POSTS, login the current user by processing the form.
    """
    if not current_user.is_authenticated:
        loginform = LoginF(request.form)
        if request.method == 'POST':
            if loginform.validate():
                username = loginform.login_email.data
                password = loginform.login_password.data
                user = db_session.query(Users).filter(Users.useremail == username).first()
                if user and user.check_password(password):
                    user.is_authenticated = True
                    login_user(user)
                    next = request.args.get('next')
                    # is_safe_url should check if the url is safe for redirects.
                    if not is_safe_url(next):
                        return abort(400)
                else:
                    flash(lazy_gettext(u'wrongcredentials'), 'error')

    return redirect(redirect_url())

@app.route("/passChange", methods=['POST', 'GET'])
@login_required
def passChange():
    """Change current user's password."""
    form = PasswordChangeF(request.form)
    if request.method == 'POST' and form.validate():
        user = current_user
        if user.check_password(form.oldpassword.data):
            user.set_password(form.newpassword.data)
            db_session.add(user)
            db_session.commit()
            flash(lazy_gettext(u'passwordchanged'), 'info')
            return redirect(redirect_url())
        else:
            flash(lazy_gettext(u'wrongpassword'), 'error')
    g.content['passchangeform'] = form
    return render_template('passchange.jinja2', **g.content)

@app.route("/addUser", methods=['POST', 'GET'])
def addUser():
    """Add new user."""
    form = AddUserF(request.form)
    if request.method == 'POST' and form.validate():
        usr = Users()
        usr.name = form.name.data
        usr.surname = form.surname.data
        usr.usertel = form.usertel.data
        usr.useremail = form.email.data
        usr.set_password(form.password.data)
        db_session.add(usr)
        try:
            db_session.commit()
        except:
            flash(lazy_gettext(u'usercannotbeaddedusernameexists'), 'error')
            return redirect(redirect_url())
        flash(lazy_gettext(u'userwasadded'), 'info')
        return redirect(redirect_url())
        
    g.content['adduserform'] = form
    return render_template('adduser.jinja2', **g.content)

@app.route("/logout", methods=['POST', 'GET'])
@login_required
def logout():
    """Logout the current user."""
    user = current_user
    user.is_authenticated = False
    db_session.add(user)
    db_session.commit()
    logout_user()
    return redirect(redirect_url())

@app.route('/')
def index():
    return render_template('index.jinja2', **g.content)

@app.route('/download/<filename>', methods=['GET', 'POST'])
def download(filename):
    uploads = os.path.join(current_app.root_path, 'files')
    print(uploads, filename)
    return send_from_directory(directory=uploads, filename=filename)
