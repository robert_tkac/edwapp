# -*- coding: utf-8 -*-
from flask import (render_template, redirect, url_for, request, g)
from . import bp
from edwapp import app
from .models import ObservationT
from edwapp.observation.forms import ObservationF
from edwapp.database import db_session


@bp.route('/', methods=["POST", "GET"])
def observation():

    form = ObservationF(request.form)
    if request.method == 'POST' and form.validate():
        observ = ObservationT()
        observ.name = form.name.data
        observ.lastname = form.lastname.data
        observ.location = form.location.data
        observ.safeact = form.safeact.data
        observ.unsafeact = form.unsafeact.data
        observ.unsafecondition = form.unsafecondition.data
        observ.nearmiss = form.nearmiss.data
        observ.qualityfault = form.qualityfault.data
        observ.environmentalissue = form.environmentalissue.data
        observ.observed = form.observed.data
        observ.risclevel = form.risclevel.data
        observ.EDW = form.EDW.data
        observ.guest = form.guest.data
        observ.actiontaken = form.actiontaken.data
        observ.escalation = form.escalation.data
        observ.howandwhoto = form.howandwhoto.data
        observ.rootcause = form.rootcause.data
        observ.chemicalcontact = form.chemicalcontact.data
        observ.crushing = form.crushing.data
        observ.cuttingsevering = form.cuttingsevering.data
        observ.drawingintrapping = form.drawingintrapping.data
        observ.electricity = form.electricity.data
        observ.entanglement = form.entanglement.data
        observ.fall = form.fall.data
        observ.fallingobject = form.fallingobject.data
        observ.frictionabrasion = form.frictionabrasion.data
        observ.hazardousenergyexposure = form.hazardousenergyexposure.data
        observ.highpresurefluidinjection = form.highpresurefluidinjection.data
        observ.impact = form.impact.data
        observ.na = form.na.data
        observ.noise = form.noise.data
        observ.shearing = form.shearing.data
        observ.slip = form.slip.data
        observ.spill = form.spill.data
        observ.puncture = form.puncture.data
        observ.strain = form.strain.data
        observ.struck = form.struck.data
        observ.suffocation = form.suffocation.data
        observ.thermaleffect = form.thermaleffect.data
        observ.trip = form.trip.data
        observ.vibration = form.vibration.data
        observ.materialhandling = form.materialhandling.data
        observ.ppeusing = form.ppeusing.data
        observ.waste = form.waste.data
        observ.nosafetyrules = form.nosafetyrules.data
        observ.other = form.other.data
        observ.fireprotection = form.fireprotection.data
        observ.contractor = form.contractor.data
        observ.wrongstandardisation = form.wrongstandardisation.data
        observ.s5s6 = form.s5s6.data
        observ.notsolvedrisk = form.notsolvedrisk.data
        observ.security = form.security.data
        observ.othernotes = form.othernotes.data
        db_session.add(observ)
        db_session.commit()
        flash('observation saved', 'info')
        return redirect(url_for('observation.observation'))
    g.content['form'] = form
    return render_template('observation.jinja2', **g.content)