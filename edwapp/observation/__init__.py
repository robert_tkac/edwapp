# -*- coding: utf-8 -*-

from flask import Blueprint

bp = Blueprint('observation', __name__, 
               url_prefix='/observation',
               template_folder='templates',
               static_folder='static')

from . import views, models