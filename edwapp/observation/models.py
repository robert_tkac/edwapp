# -*- coding: utf-8 -*-
#!/usr/bin/env python3

import datetime
from sqlalchemy import (Column, String, Integer, DateTime, Text, CheckConstraint, Boolean)
from edwapp import database


class ObservationT(database.Base):
    __tablename__ = 't_observation'
    idobserve = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(String(50), nullable=False)
    lastname = Column(String(100), nullable=False)
    date = Column(DateTime, nullable=False, default=datetime.datetime.utcnow)
    location = Column(String(10), nullable=False)
    safeact = Column(Integer, default=0)
    unsafeact = Column(Integer, default=0)
    unsafecondition = Column(Integer, default=0)
    nearmiss = Column(Integer, default=0)
    qualityfault = Column(Integer, default=0)
    environmentalissue = Column(Integer, default=0)
    observed = Column(Text)
    risclevel = Column(Integer)
    EDW = Column(Integer, default=0)
    guest = Column(Integer, default=0)
    actiontaken = Column(Text)
    escalation = Column(Integer, default=0)
    howandwhoto = Column(Text)
    rootcause = Column(Text)
    chemicalcontact = Column(Integer)
    crushing = Column(Integer)
    cuttingsevering = Column(Integer)
    drawingintrapping = Column(Integer)
    electricity = Column(Integer)
    entanglement = Column(Integer)
    fall = Column(Integer)
    fallingobject = Column(Integer)
    frictionabrasion = Column(Integer)
    hazardousenergyexposure = Column(Integer)
    highpresurefluidinjection = Column(Integer)
    impact = Column(Integer)
    na = Column(Integer)
    noise = Column(Integer)
    shearing = Column(Integer)
    slip = Column(Integer)
    spill = Column(Integer)
    puncture = Column(Integer)
    strain = Column(Integer)
    struck = Column(Integer)
    suffocation = Column(Integer)
    thermaleffect = Column(Integer)
    trip = Column(Integer)
    vibration = Column(Integer)
    materialhandling = Column(Integer)
    ppeusing = Column(Integer)
    waste = Column(Integer)
    nosafetyrules = Column(Integer)
    other = Column(Integer)
    fireprotection = Column(Integer)
    contractor = Column(Integer)
    wrongstandardisation = Column(Integer)
    s5s6 = Column(Integer)
    notsolvedrisk = Column(Integer)
    security = Column(Integer)
    othernotes = Column(Text)

    __table_args__ = (CheckConstraint('risclevel in (0,1,3)'),
                                      {'sqlite_autoincrement': True})
