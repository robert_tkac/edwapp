#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from wtforms import (Form, SelectField, StringField, BooleanField, TextAreaField, IntegerField, SubmitField)
from wtforms.validators import Length
from edwapp import lazy_gettext

class ObservationF(Form):
    '''
    Belongs to observation app
    '''
    name = StringField(lazy_gettext(u'name'), validators=[Length(min=2, message=lazy_gettext(u'errorname')), ])
    lastname = StringField(label=lazy_gettext(u'lastname'), validators=[Length(min=2, message=lazy_gettext(u'errorlastname')), ])
    locationpick = SelectField(label=lazy_gettext(u'locationpick'), choices=[('U1A', 'U1A'), 
	                                                                      ('U1B', 'U1B'), 
																		  ('U1C', 'U1C'), 
																		  ('U1D', 'U1D'),
																		  ('U2A', 'U2A'),
																		  ('U2B', 'U2B')], render_kw={"placeholder": lazy_gettext(u'pickavalue')})
    location = StringField(label=lazy_gettext(u'location'), validators=[Length(min=2, message=lazy_gettext(u'errorlocation')), ])
    safeact = BooleanField(label=lazy_gettext(u'safeact'))
    unsafeact = BooleanField(label=lazy_gettext(u'unsafeact'))
    unsafecondition = BooleanField(label=lazy_gettext(u'unsafecondition'))
    nearmiss = BooleanField(label=lazy_gettext(u'nearmiss'))
    qualityfault = BooleanField(label=lazy_gettext(u'qualityfault'))
    environmentalissue = BooleanField(label=lazy_gettext(u'environmentalissue'))
    observed = TextAreaField(label=lazy_gettext(u'observered'))
    risclevel = SelectField(label=lazy_gettext(u'risclevel'), choices=[('2', lazy_gettext(u'high')), ('1', lazy_gettext(u'medium')), ('0', lazy_gettext(u'low'))], default='0')
    EDW = BooleanField(label=lazy_gettext(u'EDW'))
    guest = BooleanField(label=lazy_gettext(u'guest'))
    actiontaken = TextAreaField(label=lazy_gettext(u'actiontaken'))
    escalation = SelectField(label=lazy_gettext(u'esclationneeded'), choices=[('0', lazy_gettext(u'no')), ('1', lazy_gettext(u'yes'))], default='0')
    howandwhoto = TextAreaField(label=lazy_gettext(u'howandwhoto'))
    rootcause = TextAreaField(label=lazy_gettext(u'rootcause'))
    chemicalcontact = BooleanField(label=lazy_gettext(u'chemicalcontact'))
    crushing = BooleanField(label=lazy_gettext(u'crushing'))
    cuttingsevering = BooleanField(label=lazy_gettext(u'cutting'))
    drawingintrapping = BooleanField(label=lazy_gettext(u'drawing'))
    electricity = BooleanField(label=lazy_gettext(u'electric'))
    entanglement = BooleanField(label=lazy_gettext(u'entaglement'))
    fall = BooleanField(label=lazy_gettext(u'fall'))
    fallingobject = BooleanField(label=lazy_gettext(u'falling'))
    frictionabrasion = BooleanField(label=lazy_gettext(u'friction'))
    hazardousenergyexposure = BooleanField(label=lazy_gettext(u'hazardous'))
    highpresurefluidinjection = BooleanField(label=lazy_gettext(u'highpressure'))
    impact = BooleanField(label=lazy_gettext(u'impact'))
    na = BooleanField(label=lazy_gettext(u'na'))
    noise = BooleanField(label=lazy_gettext(u'noise'))
    shearing = BooleanField(label=lazy_gettext(u'shearing'))
    slip = BooleanField(label=lazy_gettext(u'slip'))
    spill = BooleanField(label=lazy_gettext(u'spill'))
    puncture = BooleanField(label=lazy_gettext(u'puncturre'))
    strain = BooleanField(label=lazy_gettext(u'strain'))
    struck = BooleanField(label=lazy_gettext(u'struck'))
    suffocation = BooleanField(label=lazy_gettext(u'suffocation'))
    thermaleffect = BooleanField(label=lazy_gettext(u'thermaleffect'))
    trip = BooleanField(label=lazy_gettext(u'trip'))
    vibration = BooleanField(label=lazy_gettext(u'vibration'))
    materialhandling = BooleanField(label=lazy_gettext(u'materialhandling'))
    ppeusing = BooleanField(label=lazy_gettext(u'ppeusing'))
    waste = BooleanField(label=lazy_gettext(u'waste'))
    nosafetyrules = BooleanField(label=lazy_gettext(u'nosafetyrules'))
    other = BooleanField(label=lazy_gettext(u'other'))
    fireprotection = BooleanField(label=lazy_gettext(u'fireprotection'))
    contractor = BooleanField(label=lazy_gettext(u'contractor'))
    wrongstandardisation = BooleanField(label=lazy_gettext(u'wrongstandards'))
    s5s6 = BooleanField(label=lazy_gettext(u'5s6s'))
    notsolvedrisk = BooleanField(label=lazy_gettext(u'notsolvedrisk'))
    security = BooleanField(label=lazy_gettext(u'security'))
    othernotes = TextAreaField(label=lazy_gettext(u'othernotes'))
    submit = SubmitField(label=lazy_gettext(u'submit'))