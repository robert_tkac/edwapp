$( function() { $( ".datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' }); });

function langswitch(code){
    var loc = window.location
    var url = loc.origin + loc.pathname + "?lang=" + code;
    var element = document.getElementById(code);
    element.setAttribute("href",url);
}

function closeModal(idmodal, idform) {
    document.getElementById(idmodal).style.display = "none";
    try{ document.getElementById(idform).reset(); 
    document.getElementById(idform).style.display = "none";
    }catch(err){};
}

function showModal(idmodal, idform) {
    document.getElementById(idmodal).style.display = "block";
    document.getElementById(idform).style.display = "block";
    document.getElementsByClassName('modalfirst')[0].focus();
}