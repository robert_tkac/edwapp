import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from . import app
from .models import TransportConfigT
from .database import db_session


#SMTP Server Information
#1. Gmail.com: smtp.gmail.com:587
#2. Outlook.com: smtp-mail.outlook.com:587
#3. Office 365: outlook.office365.com
#Please verify your SMTP settings info.

#https://docs.microsoft.com/en-us/exchange/mail-flow-best-practices/how-to-set-up-a-multifunction-device-or-application-to-send-email-using-office-3

def sendEmail(subject, bodytext, emailTo=[], emailCc=''):
    COMMASPACE = ','
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_FROM').first().cvalue
    msg['BCc'] = COMMASPACE.join(emailTo)
    msg['Cc'] = COMMASPACE.join(emailCc)
    #msg.preamble = 'RFQ sent form AtlasCopco Edwards'
    #bt = (db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='EMAIL_BODY').first().cvalue).format(bodytext)
    msg.attach(MIMEText(bodytext, 'html'))
    mailserver = smtplib.SMTP(db_session.query(TransportConfigT).filter(TransportConfigT.clabel=='SMTP_SERVER').first().cvalue)
    mailserver.set_debuglevel(0)
    mailserver.send_message(msg)
    mailserver.quit()
    return 1