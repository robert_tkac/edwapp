# -*- coding: utf-8 -*-

from flask import Flask, render_template
from flaskext.csrf import csrf
from flask_babel import Babel, lazy_gettext
from flask_login import LoginManager


def page_not_found(e):
  return render_template('404.jinja2'), 404

def forbidden_handler(e):
  return render_template('403.jinja2'), 403

def unauthorized_handler(e):
  return render_template('401.jinja2'), 401

def bad_request(e):
  return render_template('400.jinja2'), 400

app = Flask(__name__, 
           instance_relative_config=True,
           template_folder='templates')
app.config.from_object('config.production')
app.config.from_pyfile('config.py')
app.register_error_handler(404, page_not_found)
app.register_error_handler(403, forbidden_handler)
app.register_error_handler(401, unauthorized_handler)
app.register_error_handler(400, bad_request)
csrf(app)
babel = Babel(app)
login_manager = LoginManager(app)
login_manager.init_app(app)


from . import database, models, views, observation, transportorder
app.register_blueprint(observation.bp)
app.register_blueprint(transportorder.bp)


@app.teardown_appcontext
def shutdown_session(exception=None):
    database.db_session.remove()