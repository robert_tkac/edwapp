#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from wtforms import (Form, StringField, PasswordField, SubmitField)
from wtforms.fields.html5 import EmailField
from wtforms.validators import InputRequired, Length, EqualTo, Email
from . import lazy_gettext

class AddUserF(Form):
    name = StringField(label=lazy_gettext(u'name'), validators=[Length(min=1, message=lazy_gettext(u'entername')),], render_kw={'autofocus': True, 'class':'first required'})
    surname = StringField(label=lazy_gettext(u'lastname'), validators=[Length(min=1, message=lazy_gettext(u'entersurname')),], render_kw={'class':'required'})
    usertel = StringField(label=lazy_gettext(u'tel'))
    email = EmailField(label=lazy_gettext(u'email'), validators=[Email(message=lazy_gettext(u'notemail'),)], render_kw={'class':'required'})
    password = PasswordField(label=lazy_gettext(u'password'), validators=[Length(min=2, max=15), EqualTo('confirm', message=lazy_gettext(u'passwordmustmatch'))])
    confirm = PasswordField(lazy_gettext(u'repeatpassword'), validators=[Length(min=2, max=15), EqualTo('password', message=lazy_gettext(u'passwordmustmatch'))])
    submit = SubmitField(lazy_gettext(u'adduser'), render_kw={'class':'commonSubmit'})


class LoginF(Form):
    login_email = StringField(label=lazy_gettext(u'email'),validators=[InputRequired(),], render_kw={'class':'required modalfirst'})
    login_password = PasswordField(label=lazy_gettext(u'password'), validators=[InputRequired(),], render_kw={'class':'required'})
    login_submit = SubmitField(lazy_gettext(u'submit'), render_kw={'class':'commonSubmit'})


class PasswordChangeF(Form):
    oldpassword = PasswordField(label=lazy_gettext(u'oldpassword'), validators=[Length(min=2)], render_kw={'autofocus': True, 'class':'first'})
    newpassword = PasswordField(label=lazy_gettext(u'newpassword'), validators=[Length(min=2, max=15), EqualTo('confirm', message=lazy_gettext(u'passwordmustmatch'))])
    confirm = PasswordField(lazy_gettext(u'repeatpassword'), validators=[Length(min=2, max=15), EqualTo('newpassword', message=lazy_gettext(u'passwordmustmatch'))])
    submit = SubmitField(lazy_gettext(u'submit'), render_kw={'class':'commonSubmit'})